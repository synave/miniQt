# miniQt

Programme minimal en Qt montrant l'utilisation des calques, des étiquettes, des zones de texte et des boutons. Le programme illustre également la connexion entre le clic d'un bouton et l'exécution d'une méthode.

Prérequis
---------
Avoir installé la bibliothèque Qt avec Debian 11 et Ubuntu 22.04
```
apt-get install qtbase5-dev qtchooser
```

Avoir instamllé la bibliothèque Qt avec Debian 12
```
apt-get install qt6-base-dev qmake6
```

Compilation/exécution
---------------------
Pour Debian 11 et Ubuntu 22.04 :
```
cd src
qmake
make
./miniQt
```

Pour Debian 12 :
```
cd src
qmake6
make
./miniQt
```

Suppression des fichiers de compilation
---------------------------------------
```
cd src
make clean
```

Suppression totale des fichiers de compilation
---------------------------------------
```
cd src
make distclean
```