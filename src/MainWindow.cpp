#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>

#include "MainWindow.hpp"


MainWindow::MainWindow ()
{
  setWindowTitle (tr ("miniQt"));

  // Création de la zone de travail centrale
  QWidget *central = new QWidget (this);
  setCentralWidget (central);

  // On va organiser les différents éléments dans un
  // calque vertical
  QVBoxLayout *vLayout = new QVBoxLayout (central);
  // Calque qu'on assigne à la zone de travail centrale
  central->setLayout (vLayout);

  // Création d'un label
  label = new QLabel ("Coucou !", central);
  // qu'on centre
  label->setAlignment (Qt::AlignCenter);
  // et qu'on ajoute à la première ligne de notre calque
  vLayout->addWidget (label);

  // La seconde ligne de notre calque vertical sera composé
  // de plusieurs éléments les uns derrière les autres.
  // On crée donc un calque horizontal
  QHBoxLayout *hLayout = new QHBoxLayout (central);
  // qu'on affecte à la seconde ligne de notre calque vertical
  vLayout->addLayout (hLayout);

  // On crée une zone de texte
  text = new QTextEdit (QString ("Tu veux voir ma"), central);
  // dont on fixe la taille en hauteur
  text->setMaximumHeight (27);
  // et qu'on assigne à la première case de notre
  // calque horizontal (donc dans la seconde ligne
  // du calque vertical
  hLayout->addWidget (text);

  // On crée un bouton aevc le texte "cliquez moi !"
  button = new QPushButton ("cliquez moi !", central);
  // qu'on ajoute à la suite de la zone de texte
  // dans le calque horizontal - donc toujours dans
  // la seconde ligne du calque vertical
  hLayout->addWidget (button);

  // On crée un dernier label qu'on ajoute à la suite
  // du bouton dans le calque horizontal...
  label2 = new QLabel ("?", central);
  hLayout->addWidget (label2);

  // Création de la connexion entre le clic sur le bouton
  // et l'exécution de la méthode clic.
  // Cette méthode sera appelée lors du clic sur le bouton.
  connect (button, SIGNAL (clicked (bool)), this, SLOT (clic ()));
}

void
MainWindow::clic ()
{
  // Lors du clic sur le bouton, cette méthode est
  // appelée et modifie le texte sur le bouton.
  button->setText ("belle interface graphique");
}
