QT += core gui widgets

HEADERS = MainWindow.hpp
                
SOURCES = MainWindow.cpp \
          main.cpp

# install
target.path = $$[QT_INSTALL_EXAMPLES]/miniQt
INSTALLS += target
